---
title : "Routard du pharmacien"
lang: fr
---

Page compilée le <today> — version à jour disponible sur [framagit.org](https://prep.frama.io/routard/routard.html).

Le projet est disponible [ici](https://framagit.org/prep/routard).

*It's dangerous to go alone, use this !*

# Pharmacie

## La base

### Les RCP

* [EMA](https://www.ema.europa.eu/en/medicines) : regarder la *scientific discussion* +++
* [FDA](https://www.accessdata.fda.gov/scripts/cder/daf/index.cfm) : regarder la *review* +++
* [Thériaque](http://www.theriaque.org/apps/contenu/accueil.php)
* [Compendium suisse des médicaments](https://compendium.ch/home/fr), parfois plus complet que le RCP français
* [Santé Canada](https://produits-sante.canada.ca/dpd-bdpp/switchlocale.do?lang=fr&url=t.search.recherche)
* [MHRA](https://products.mhra.gov.uk/) : UK
* [AFMPS](http://bijsluiters.fagg-afmps.be/?localeValue=fr) : Belgique

### Divers

* [Micromedex ](https://www.micromedexsolutions.com/micromedex2/librarian/ssl/true) : accès via l'AP-HP
* [Drugbank](https://www.drugbank.ca/)
* [Abréviations en médecine](https://fr.wikipedia.org/wiki/Liste_d'abr%C3%A9viations_en_m%C3%A9decine)
* [Couper - écraser](http://lmgtfy.com/?iie=1&q=couper+%C3%A9craser+(PDF))
* [Specialist Pharmacy Service](https://www.sps.nhs.uk/) : l'omedit anglais, en **beaucoup** mieux

### La Suisse -- équivalences

* [Pharmacie des HUG](https://pharmacie.hug-ge.ch/infos-medicaments/recommandations-d-utilisation)
* [Pharmacie Interhospitalière de la Côte (PIC)](http://pharmpic.ch/)
* [Pharmacie interjurassienne](http://www.pij-medic.info/informations/infos-pratiques-et-procedure) (équivalences) #godtier

## /adapt/ adaptations posologiques

* [Site GPR](https://sitegpr.com) : insuffisance rénale (ceux qui utilisent Cockcroft sont fans de Phil Collins)
* [DDI Predictor](https://www.ddi-predictor.org/) : cirrhose et cytochromes

## /dm/ dispositifs médicaux

* [europharmat](http://www.euro-pharmat.com/)
* [Chemical Resistance of Thermoplastics](https://www.sciencedirect.com/science/book/9781455778966) : résistance des plastiques à pleins de choses, ~ 3500 pages


## /hyg/ hygiène

* [ProHyBase](http://www.prodhybase.fr/)
* [Répias](https://www.preventioninfection.fr/base-documentaire/)

## /iam/ interactions médicamenteuses

* Bases Liverpool : ressources sur la pk, les interactions
	* [AAD](https://www.hep-druginteractions.org/)
	* [ARV](https://www.hiv-druginteractions.org/)
	* [Chimios](https://cancer-druginteractions.org/)
* [Antifongiques](http://umc.osage1.nl/tool)
* CYP 450: [1](https://www.hug-ge.ch/sites/interhug/files/structures/pharmacologie_et_toxicologie_cliniques/a5_cytochromes_6_2.pdf), [2](https://drug-interactions.medicine.iu.edu/main-table.aspx)
* [Pharmgkb](https://www.pharmgkb.org/) : pharmacogénomique (NIH & Standford university)
* [QT drug list](https://www.crediblemeds.org/healthcare-providers/) : créer un compte gratuit
* [Transporteurs rénaux](https://www.ncbi.nlm.nih.gov/pubmed/28210973) : publi avec un gros tableau avec tous les médicaments
* [*Drug Interaction Probability Scale (DIPS)*](http://www.pmidcalc.org/?sid=17389673)

## /iap/ interactions phytothérapie

Chercher la plante, ses molécules actives, leurs effets

* [Hedrine](https://hedrine.ujf-grenoble.fr/)
* [Livret AFSOS](http://www.afsos.org/actualites-evenements/livret-phytotherapie-volume-1/)
* [*about herbs*](https://www.mskcc.org/cancer-care/diagnosis-treatment/symptom-management/integrative-medicine/herbs) ([MSKCC](https://en.wikipedia.org/wiki/MSKCC))
* [EMA herbal products](https://www.ema.europa.eu/en/human-regulatory/herbal-medicinal-products)
* [EMA monographs](https://www.ema.europa.eu/en/medicines/field_ema_web_categories%253Aname_field/Herbal)
* [HerbMed](http://www.herbmed.org/index.html)
* [NIH](https://nccih.nih.gov/health/herbsataglance.htm), [NIH reviews](https://nccih.nih.gov/health/providers/litreviews.htm)
* Littérature: [review 1](https://onlinelibrary.wiley.com/doi/epdf/10.1002/cncr.29796)

## /inj/ injectables : reconstitution, administration, compatibilité

* [HUG](https://pharmacie.hug-ge.ch/infos-medicaments/recommandations-d-utilisation) : la base
* anti-infectieux: [fulltext](http://www.omedit-idf.fr/wp-content/uploads/2017/08/prep-et-adm-ATB.pdf), [DOI](https://doi.org/10.1016/j.medmal.2016.01.010) #godtier
* administration sous-cutanée : [omedit poitou](https://omedit.esante-poitou-charentes.fr/portail/gallery_files/site/136/5131/5135/5212.pdf)
* [Pharmacie Interhospitalière de la Côte (PIC)](https://www.micromedexsolutions.com/micromedex2/librarian/ssl/true)
* extravasation : [HUG](https://pharmacie.hug-ge.ch/infomedic/utilismedic/extravasation_non_cyto.pdf)
	* appli igr pour les chimios

## /ped/

* [EMA guideline on pharmaceutical development of medicines for paediatric use](https://www.ema.europa.eu/documents/scientific-guideline/guideline-pharmaceutical-development-medicines-paediatric-use_en.pdf)
* [EuPFI (European Paedriatric Formulation Initiative)](http://www.eupfi.org)
	* [STEP Database (*Safety & Toxicity of Excipients for Paediatrics*)](https://step-db.ucl.ac.uk)
* [GRIP (*Global Research in Paediatrics*)](http://www.grip-network.org)
* [BPCA (*Best Pharmaceuticals for Children Act*)](https://bpca.nichd.nih.gov)

## /org/ organisation

Gestion des risque, etc.

* [Sécu des patients (CH)](http://www.patientensicherheit.ch/fr/actualite.html), encore une fois les suisses déboitent ; voir la section « publications »
* [INRS](http://www.inrs.fr/)
* [Lean en PUI](https://pastel.archives-ouvertes.fr/tel-01127366)

## /pim/ : prescription potentiellement inappropriées

* pédiatrie : [POPI](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0101171) #RDB
* 45-64 ans : [PROMPT](https://link.springer.com/article/10.1007%2Fs00228-015-2003-z)
* Gériatrie : listes de [Laroche](http://www.omeditbretagne.fr/lrportal/documents/138946/151314/laroche-prescriptions-medict-inappropriees-pa-laroche-rev-med-interne-2009.pdf/a71e41e3-6557-447b-9538-67ab4fbdb4b5), [Beers](https://onlinelibrary.wiley.com/doi/pdf/10.1111/jgs.13702) et [STOPP/START](https://academic.oup.com/ageing/article/44/2/213/2812233#page=1&view=FitH)
* [PIM-Check](http://app.pimcheck.org/#/accueil)

## /ptk/

### La base

* [Pharmacopée Européenne](http://online.edqm.eu.proxy.scd.u-psud.fr/FR/entry.htm)
* [JP](http://www.pmda.go.jp/english/rs-sb-std/standards-development/jp/0010.html) : la seule en accès libre, en anglais ; contient des spectres IR et UV
* [Pharmacopée internationale (OMS)](http://apps.who.int/phint/2018/index.html#p/home) (en) : contient des spectres IR/UV
* [ICH](http://www.ich.org/products/guidelines/quality/article/quality-guidelines.html), en français sur [Santé Canada](https://www.canada.ca/fr/sante-canada/services/medicaments-produits-sante/medicaments/demandes-presentations/lignes-directrices/international-conference-harmonisation/qualite.html)
* [PIC/Scheme](https://www.picscheme.org/en/publications)

### Matières premières

* [Pharmacompass](https://www.pharmacompass.com/)
* [CEP](https://extranet.edqm.eu/publications/recherches_CEP.shtml)
* [DMF](https://www.fda.gov/drugs/developmentapprovalprocess/formssubmissionrequirements/drugmasterfilesdmfs/default.htm)
* [Excipients Database (FDA)](https://www.accessdata.fda.gov/scripts/cder/iig/index.cfm)

### Techniques

* [FDA dissolution](https://www.accessdata.fda.gov/scripts/cder/dissolution/index.cfm)
* SFSTP : [guide](https://www.sciencedirect.com/science/article/abs/pii/S0731708504003292) de validation de méthode analytique, 4 parties

### Réglementaire

* [Base EudraGMDP](http://eudragmdp.ema.europa.eu/inspections/displayHome.do) : recherche des certificats GMP/GDP
* [*EMA Scientific guidelines*](http://www.ema.europa.eu/ema/index.jsp?curl=pages/regulation/general/general_content_000043.jsp&mid=WC0b01ac05800240cb)
* [*EMA Quality guidelines*](http://www.ema.europa.eu/ema/index.jsp?curl=pages/regulation/general/general_content_000081.jsp&mid=WC0b01ac0580027546) : reprennent et complètent les ICH

* excipients : 
	* [JECFA](http://apps.who.int/food-additives-contaminants-jecfa-database/search.aspx) : reco OMS
	* [EFSA](https://webgate.ec.europa.eu/foods_system/main/?event=display) : reco UE

## /pv/ pharmacovigilance

* [CERFA EI](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_10011.do) 
* [CRAT](http://lecrat.fr/) : centre de référence sur les agents tératogènes

## /rph/ radiopharmacie

* LNHB : données atomiques et nucléaires
	* [table](http://www.lnhb.fr/donnees-nucleaires/donnees-nucleaires-tableau/)

## /reg/ réglementaire

* [EMA : médicaments en évaluation](https://www.ema.europa.eu/en/about-us/how-we-work/what-we-publish/medicines-under-evaluation)

## /stab/ stabilités

* [Stabilis](http://www.stabilis.org/Monographie.php?Liste)
	* module détection d'incompatibilité
	* Thériaque peut aussi le faire
* Le Trissel's *Handbook of injectable drugs*
* [EMA](https://www.ema.europa.eu/en/medicines) : voir les  *Scientific discussion*

Pour les chimios voir section [/po/](#po-pharmacie-oncologique)

## /stat/ statistiques

* [BiostaTGV](http://marne.u707.jussieu.fr/biostatgv/)
* [CombiStat](https://www.edqm.eu/en/combistats)

## /tox/ toxicologie

* [Pneumotox](http://www.pneumotox.com/)
* [Livertox](https://livertox.nih.gov/)
* [Rein](http://sitegpr.com/fr/)
* [LactMed (allaitement)](https://toxnet.nlm.nih.gov/newtoxnet/lactmed.htm)
* [Vidal fiches toxiques](http://evidal.vidal.fr.proxy.scd.u-psud.fr/toxin)
* [Toxnet Databases](https://toxnet.nlm.nih.gov/)
* [Specialized Information Service](https://sis.nlm.nih.gov/chemical.html)
* [erowid](https://www.erowid.org/) : addicto

# Cancérologie

* Appli mobile de l'IGR (manuel de l'interne)

## /po/ pharmacie oncologique

### /manip/ manipulation des cytotoxiques

* [ISOPP](http://www.isopp.org/isopp-education-resources)
* [SFPO](https://www.sfpo.com)
* [ASCO](https://ascopubs.org/doi/full/10.1200/JCO.18.01616)
* Guide chimios CNHIM : à quand un pdf ?
* [INRS](http://www.inrs.fr/media.html?refINRS=ND%202201)
* [NIOSH](http://www.isopp.org/isopp-education-resources), INRS in 'murica
* [EU-OSHA](https://osha.europa.eu/fr), INRS européen
* Recos de stabilité SFPO/ESOP
	* [2013](https://doi.org/10.1016/j.pharma.2013.06.002)
	* [2011](https://doi.org/10.1016/j.pharma.2011.07.002)

### /adap/ Adaptations

* [Lancet, 2019](https://www.thelancet.com/journals/lanonc/article/PIIS1470-2045(19)30145-7/fulltext)


### /cpo/ Chimios per os

* [Oncolien (SFPO)](http://oncolien.sfpo.com/)
* [INCa, fiche chimio po](http://www.e-cancer.fr/Professionnels-de-sante/Recommandations-et-outils-d-aide-a-la-pratique/Anticancereux-par-voie-orale) (version pharmacien, mieux que les omedit)
* [Guide chimio per os ASCO](http://ascopubs.org/doi/pdf/10.1200/JOP.2013.001183)
* [Guide chimio per os CCLG (UK)](https://www.cclg.org.uk/publications/Cancer-drugs-factsheets)
* NHS :
	* [The Oral Anticancer Medicines Handbook](http://www.necn.nhs.uk/wp-content/uploads/2012/11/OralChemoHandbook-Finalv3-0b2.pdf)
	* [Oral Systemic Anticancer Therapies Counselling Handbook](http://www.londoncanceralliance.nhs.uk/media/122917/lca-oral-sact-counselling-handbook-amended-march-2016.pdf)


## /ref/ référentiels


* [Oncologik](http://oncologik.fr/referentiels/ordre-anatomique), [oncomip](http://oncomip.org/fr/espace-professionnel/referentiels/referentiels-thesaurus.html), [oncauvergne](http://www.oncauvergne.org/index.php?option=com_content&view=article&id=59&Itemid=4), [oncobretagne](https://www.oncobretagne.fr/groupes-techniques/), [oncocentre](http://oncocentre.org/professionnels/structures-de-specialites/), [onconormand](http://oncocentre.org/professionnels/structures-de-specialites/)
* Autre : [TNCDigestive (SNFGE)](https://www.snfge.org/tncd)
* [Onkopedia](https://www.onkopedia-guidelines.info/en/onkopedia/guidelines) : reco allemandes en anglais, bonnes synthèses

## /ss/ soins de support

* [AFSOS](http://www.afsos.org/)
* [MASCC](http://www.mascc.org/practice-resources)

## /sp/ santé publique

* [Monographies du CIRC](http://monographs.iarc.fr/ENG/Monographs/PDFs/index.php)
* [*Report on Carcinogens*](https://ntp.niehs.nih.gov/pubhealth/roc/index.html) : *National Toxicology Program* américain

# Médecine

* [Outil entretien patient multilingue](http://www.traducmed.fr/)
* [Médiglotte](http://mediglotte.e-monsite.com/)
* Google Trad est ton ami

* [EBM](https://www.ebmfrance.net/Pages/default.aspx) : wikipédia en mieux
* [EBP](https://www.ebpnet.be/fr/Pages/default.aspx) : EBM en belge

## /UMVF/

* [Collèges de médecine](http://campus.cerimes.fr)


## /anapath/

* [histologie](http://www.cetice.u-psud.fr/histo_anapat/)

## /cardio/

* Médicament et intervalle QT : cf [interactions médicamenteuses](#iam-interactions-médicamenteuses)
* [ECG Clic](https://ecgclic.fr/)

## /dermato/

* [Thérapeutique dermatologique](https://www.therapeutique-dermatologique.org/spip.php?rubrique1&lang=fr) : inscription gratuite
* [Dermatoclic](https://www.dermatoclic.com/)

## /dim/ information médicale

* [BDD de la CNAMTS](http://www.codage.ext.cnamts.fr/codif/bdm_it/index_presentation.php?p_site=AMELI)
* [Tarifs des GHS](https://www.atih.sante.fr/tarifs-mco-et-had)
* [Médicaments de la liste en sus](https://www.atih.sante.fr/unites-communes-de-dispensation-prises-en-charge-en-sus)
* [Abréviations PMSI](www.lespmsi.com/abreviations-pmsi-pilotage-facturation-t2a)

## /endoc/

* [Diabétoclic](http://diabetoclic.fr/)
* [GRIO](http://www.grio.org/): groupe d'étude sur l'ostéoporose
* [Thyroclic](http://aporose.fr/thyroclic/) : nodule thyroïdien

## /geria/

* [Demence clic](https://demenceclic.fr/)

## /gyn/ 

* [Formagyn](https://formagyn.fr/) : god-tier. Contraception, etc.

## /hémato/

* [Hématocell](https://hematocell.fr) (CHU Angers)
* [Aide-mémoire](http://2bib.ch/hemato/hemato-fra.pdf) (CHU Lausanne)
* [Platelets on the Web](https://ouhc.edu/platelets/index.html) : infos sur les thrombopénies, purpura, etc.
* [Thromboclic](http://www.thromboclic.fr/)

## /hge/ hépato-gastro-entérologie

* [SNFGE](https://www.snfge.org/content/bibliotheque-scientifique)
* [Collège](https://www.snfge.org/content/bibliotheque-scientifique), accès libre
* [Recos](https://www.snfge.org/recommandations)


## /immuno/

* [Allergoclic](http://allergoclic.fr/)

## /infectio/

* [SPILF](http://www.infectiologie.com/site/consensus_recos.php)
	* [Toolbox](http://www.infectiologie.com/fr/toolbox.html)
		* [Guide de prescription](http://www.infectiologie.com/fr/guides-de-prescription.html)
* le RÉMIC (microbio), 2 tomes
* [fiches internat](http://aemip.fr/?page_id=248)
* [VIHclic](https://vihclic.fr/)
* [Antibioclic](https://antibioclic.com/)

## /mg/ médecine générale

* [ReAGJIR](https://www.reagjir.fr/)
* [Diagosaurus](https://accessmedicine.mhmedical.com/Diagnosaurus.aspx?categoryid=41309&selectedletter=A)
* [Risque cardio](http://chd.bestsciencemedicine.com/calc2.html)
* [addict'aide](https://www.addictaide.fr/)

## /néphro/

* [Collège de néphrologie](http://cuen.fr/umvf/)

## /neuro/

* [Collège de neurologie](https://www.cen-neurologie.fr/)

## /obst/

* [Gestaclic](http://gestaclic.fr/)
* [embryologie](http://cvirtuel.cochin.univ-paris5.fr/Embryologie/Accueil/Accueil.htm)

## /oph/

* [ophtalmoclic](https://ophtalmoclic.fr/)



## /pedia/

* [Pédiadol](https://pediadol.org/)
* [GPIP](http://gpip.sfpediatrie.com/doc-du-gpip)
* [Antibio GPIP](http://gpip.sfpediatrie.com/sites/default/files/GPIP/arcped_gpip_15_juin_new_couv_bs.pdf)
* *Pediatric & neonatal dosage handbook*
* [PAP](https://pap-pediatrie.fr/)

## /radio/

* [Radiopaedia](https://radiopaedia.org/)
* [Pinkybone](https://www.pinkybone.com) : votre mémo de radiologie 2.0
* [Smart-radiology](https://smart-reporting.com/)
* [Neuroanat](http://www.chups.jussieu.fr/ext/neuranat/index.html)
* [Banque d'image](http://stim-imagerie.tripod.com/Accueil.htm)


## /sop/ soins palliatifs

* [Palliclic](https://palliaclic.com/)

## /sp/ santé publique

* [BEH](http://invs.santepubliquefrance.fr//Publications-et-outils/BEH-Bulletin-epidemiologique-hebdomadaire)
* [Conseils aux voyageurs](http://invs.santepubliquefrance.fr/Dossiers-thematiques/Populations-et-sante/Voyageur-s-Recommandations-sanitaires-aux-voyageurs)
* [European CDC](https://ecdc.europa.eu/en/home)
    * [ECDC Field Epidemiology Manual](https://wiki.ecdc.europa.eu/fem/Pages/FEM Wiki home.aspx)
* [Vaccination Info Service](https://professionnels.vaccination-info-service.fr/) : site pro

Base de données :

* [dress](http://www.data.drees.sante.gouv.fr/ReportFolders/reportFolders.aspx?sCS_referer=&sCS_ChosenLang=fr)
* [oms](http://apps.who.int/gho/data/?theme=main)
* [eurostat](https://ec.europa.eu/eurostat/web/main/home)

# Biologie

## Manuels de prélèvements

* [Henri Mondor](https://chu-mondor.manuelprelevement.fr/)
* [IGR](https://gustaveroussy.manuelprelevement.fr/)

## /para/ parasitologie

* CDC: [DPDx](https://www.cdc.gov/dpdx/index.html)


# Biblio

## Find that goddam paper

<div id="raven">

![](raven.png "raven"){width=3cm}

</div>

Un pdf n'est pas accessible ?

* Sci-Hub : [.hk](https://sci-hub.se), [liens 1](http://sci-hub.tech/), [liens 2](https://lovescihub.wordpress.com/) [IP 1](https://80.82.77.83), [IP 2](https://80.82.77.84), [tor](https://scihub22266oqcxt.onion), [wen](https://en.wikipedia.org/wiki/Sci-Hub)
* Library Genesis [.is](https://libgen.is), [gen.lib.rus.ec](http://gen.lib.rus.ec/)
* [#ICanHazPDF](https://en.wikipedia.org/wiki/ICanHazPDF)
* [Aaron Swartz](https://en.wikipedia.org/wiki/Aaron_Swartz#Open_Access)


## Write that goddam paper

* [zotero](https://zotero.org) : use it ftw
* [EQUATOR](https://www.equator-network.org/) : toutes les checklists genre CONSORT, PRISMA, etc.
