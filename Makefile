all: routard.html

routard.html : links.md style.css raven.png
	sed "s/<today>/`date -I`/" $< > links_tmp.md
	pandoc links_tmp.md --self-contained -c style.css --toc --toc-depth=2 -o public/$@
	rm links_tmp.md
